---
layout: job_family_page
title: "Field Security Engineer"
---

The Field Security team is the public representation of GitLab's internal Security Team and function. They are responsible for evangelising GitLab's Security practices both internally and externally, as well as providing expert guidance to internal and external customers about the best practices to secure GitLab assets.

As a member of the Field Security Team at GitLab, you will be working towards raising the bar on security. We will achieve that by working and collaborating with cross-functional teams and global customers to provide guidance on security best practices.

The [Security Team](/handbook/engineering/security) is responsible for leading and
implementing the various initiatives that relate to improving GitLab's security.

## Responsibilities
- Work closely with Sales, Support and other Customer facing teams to provide subject matter expertise about GitLab's Security program and products, including conversations and responses to enquiries about how GitLab secures its products.
- Work closely with internal engineering, support and solutions architecture teams to improve their capacity to respond to security questions and incidents, as well as guide these teams to adopt improved security practices and capabilities.
- Collect and provide feedback to internal Security teams about customer needs and customer results. Escalate discovered Security issues to the appropriate internal team and follow through to completion.
- Assist Compliance teams with Security expertise, both for internal certification requirements and for customer questionnaires and contracts.
- Monitor community forums and internal Slack channels for Security related issues, and manage responses and solutions for questions asked.
- Provide presentations to GitLab teams and customers on GitLab Security

## Requirements

- You have a passion for security and open source
- You are a team player, and enjoy collaborating with cross-functional teams
- You are a great communicator
- You employ a flexible and constructive approach when solving problems
- You share our [values](/handbook/values), and work in accordance with those values
- Ability to use GitLab


## Levels

### Senior Field Security Engineer

* Leverages security expertise in at least one specialty area
* Excellent knowledge of Internet Security protocols and technologies.
* Strong industry security experience, particularly in DevOps, Application Security and/or Cloud Security
* Pre-Sales engineering experience, with demonstrated experience working with Sales teams in Enterprise or Government verticals. Experience participating and completing RFP Proposals related to IT Security
* Solid understanding of Compliance standards as they apply to Security Controls, particularly ISO27001, SOC 2, PCI DSS, FedRAMP etc.
* Excellent written and verbal communication and presentation skills


***

A Field Security Engineer may grow to pursue roles within the Security team (e.g. within Security Operations, Application Security, Security Research) or may use this as a transitional role towards other teams (e.g. Engineering, Solutions Architecture, Professional Services, Sales Engineering).  See [Engineering Career Development](/handbook/engineering/career-development) for more detail.

***

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Qualified candidates receive a short questionnaire from our Recruiting team
- Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with our Recruiting team
- Next, candidates will be invited to schedule an interview with Strategic Security Engineer/Manager
- Candidates will then be invited to schedule an interview with Application Security/Security Operations/Security Research Engineer
- Candidates will then be invited to schedule an interview with Security Compliance Manager
- Candidates will then be invited to schedule an additional interview with Sr. Director of Security
- Successful candidates will subsequently be made an offer via email

As always, the interviews and screening call will be conducted via a video call.
See more details about our hiring process on the [hiring handbook](/handbook/hiring).
